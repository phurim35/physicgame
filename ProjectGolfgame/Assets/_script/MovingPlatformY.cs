﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatformY : MonoBehaviour
{
    Vector3 MoveY = new Vector3(0, 5f, 0);
    // Update is called once per frame
    void Update()
    {
        this.transform.Translate(MoveY * Time.deltaTime);
        if(this.transform.position.y >= 40)
        {
            MoveY.y *= -1f;
        }
        if(this.transform.position.y <= 23)
        {
            MoveY.y *= -1f;
        }
        
    }
}
