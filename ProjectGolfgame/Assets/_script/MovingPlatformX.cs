﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatformX : MonoBehaviour
{
    Vector3 MoveX = new Vector3(5f, 0, 0);
    // Update is called once per frame
    void Update()
    {
        this.transform.Translate(MoveX*Time.deltaTime);
        if(this.transform.position.x >= 4)
        {
            MoveX.x *= -1f;
        }
        if(this.transform.position.x <= -20)
        {
            MoveX.x *= -1f;
        }
        
    }
}
