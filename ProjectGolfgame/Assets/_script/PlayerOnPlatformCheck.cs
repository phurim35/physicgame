﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerOnPlatformCheck : MonoBehaviour
{
    public GameObject Player;
    public GameObject PlayerCam;
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            Player.transform.parent = transform;
            
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Player.transform.parent = null;
            
        }
    }
}
